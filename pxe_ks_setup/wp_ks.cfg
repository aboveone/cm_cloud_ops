#version=DEVEL

###### Installer Configuration #####################################################
# Use network installation replace with basesearch and releasever variables
url --url="https://mirror.its.sfu.ca/mirror/CentOS/7/os/x86_64/"

# License agreement
eula --agreed

#enable EPEL in order to install additional packages
repo --name="epel" --baseurl=http://download.fedoraproject.org/pub/epel/$releasever/$basearch

# Use graphical install
graphical

#Turn up logging
logging level=debug

# Reboot after installation
reboot

#Don't run keyboard / language / location / network setup on first boot
firstboot --disable
###### End Installer Configuration #################################################

###### Locale Configuration ########################################################
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'

# System language
lang en_CA.UTF-8

# System timezone
timezone America/Vancouver --isUtc
###### End Locale Configuration ####################################################

###### User and Auth Configuration #################################################
# System authorization information
auth --passalgo=sha512 --useshadow

# Root password 
rootpw --iscrypted $6$2l//8BjxLdQyA.dr$vrprwxRjVwPdryUK/kKTvmzRrg3rBZ8irAabsNK/3wJpDtfQBHREsei5hufq8oT38TFI/npUmyND.FeUMCOha.

user --name=admin --password=$6$nVKblWF2gTFgx9z8$ossEWzzMVWClRig6ty2eSncgwcfNifZO8PzHt5rW2B7s.E6an.v/dApVYQULYWfpxOnybrH.YLalaztegE3fh0 --iscrypted --gecos="admin" --groups="wheel"

###### End User and Auth Configuration #################################################

###### Network Configuration #######################################################
network  --bootproto=static --device=enp0s3 --gateway=192.168.254.1 --ip=192.168.254.10 --nameserver=208.67.222.222 --netmask=255.255.255.0 --ipv6=auto --activate
network  --hostname=wp02.lab471.htpbcit.ca

###### End Network Configuration ###################################################

###### Disk Setup ##################################################################
ignoredisk --only-use=sda
clearpart --drives=sda --all 
autopart --type=btrfs

# System bootloader configuration (note location=mbr puts boot loader in ESP since UEFI)
bootloader --append="rhgb crashkernel=auto" --location=mbr #--driveorder=/dev/sda

###### End Disk Setup ##################################################################

###### Addons: kernel dump #############################################################
%addon com_redhat_kdump --enable --reserve-mb='auto'

%end
###### End Addons: kernel dump #########################################################

###### Security Configuration ######################################################
firewall --enabled --http --ssh --service=tftp
selinux --permissive
###### End Security Configuration ##################################################

###### System services #############################################################
services --enabled=sshd,ntpd,chronyd,nginx,php-fpm,mariadb
###### End System services #########################################################


###### Pre-Installation Script #########################################################
###### End Pre-Installation Script #####################################################

###### Package Installation ############################################################
%packages
@base
epel-release
vim
chrony
git
kernel-devel
kernel-headers
dkms
gcc
gcc-c++
kexec-tools
ntp
nginx
bzip2
php
php-mysql
php-fpm
mariadb
mariadb-server
%end
###### End Package Installation ########################################################

###### Post-Installation Script ########################################################
%post --log=/root/ks-post.log

#!/bin/bash
#Copy ssh authorized keys to new image
#Set ownership and permission of admin authorized keys
chmod -R u=rw,g=,o= /home/admin/.ssh
chown -R admin /home/admin/.ssh
chgrp -R admin /home/admin/.ssh
chmod u=rwx,g=,o= /home/admin/.ssh

#Update System
yum -y update

#Turn Down Swapiness since its an SSD disk
echo "vm.swappiness = 10" >> /etc/sysctl.conf

#Install Virtualbox Guest Additions
mkdir vbox_cd
mount /dev/sr1 ./vbox_cd
./vbox_cd/VBoxLinuxAdditions.run
umount ./vbox_cd
rmdir ./vbox_cd

#Allow all wheel members to sudo all commands without a password by uncommenting line from /etc/sudoers
sed -i 's/^#\s*\(%wheel\s*ALL=(ALL)\s*NOPASSWD:\s*ALL\)/\1/' /etc/sudoers

#Add SSH public key for user admin and root
mkdir -p /root/.ssh/
mkdir -p /home/admin/.ssh/
wget -O /root/.ssh/authorized_keys https://bitbucket.org/aboveone/cm_cloud_ops/raw/master/pxe_ks_setup/nasp18_admin_id_rsa.pub
wget -O /home/admin/.ssh/authorized_keys https://bitbucket.org/aboveone/cm_cloud_ops/raw/master/pxe_ks_setup/nasp18_admin_id_rsa.pub

#tftp configuration: enable tftp by changing disabled from yes to no
sed -i 's/\s*\(disable =\s*\)yes/\1no/' /etc/xinetd.d/tftp

#Disable CGI fix path in PHP-FPM
sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php.ini

#Set the correct PHP-FPM listen socket path
sed -i "s/listen = 127.0.0.1:9000/listen = \/var\/run\/php-fpm\/php-fpm.sock/" /etc/php-fpm.d/www.conf

#Change owner and group to nobody for security purposes
sed -i "s/;listen.owner = nobody/listen.owner = nobody/" /etc/php-fpm.d/www.conf
sed -i "s/;listen.group = nobody/listen.group = nobody/" /etc/php-fpm.d/www.conf

#Change PHP-FPM user and group to nginx
sed -i "s/user = apache/user = nginx/" /etc/php-fpm.d/www.conf
sed -i "s/group = apache/group = nginx/" /etc/php-fpm.d/www.conf

#Get the nginx config file
rm -f /etc/nginx/nginx.conf
curl -o /etc/nginx/nginx.conf https://bitbucket.org/aboveone/cm_cloud_ops/raw/master/pxe_ks_setup/nginx.conf

#Create a PHP info file for troubleshooting purposes
echo "<?php phpinfo(); ?>" > /usr/share/nginx/html/info.php

#Download and extract wordpress to nginx document root
wget -O - http://wordpress.org/latest.tar.gz | tar zxf - --strip 1 -C /usr/share/nginx/html/

#Create the Wordpress config file
cp /usr/share/nginx/html/wp-config-sample.php /usr/share/nginx/html/wp-config.php

#Insert the Database info
sed -i "s/define('DB_NAME', 'database_name_here');/define('DB_NAME', 'wordpress');/" /usr/share/nginx/html/wp-config.php
sed -i "s/define('DB_USER', 'username_here');/define('DB_USER', 'wordpress_user');/" /usr/share/nginx/html/wp-config.php
sed -i "s/define('DB_PASSWORD', 'password_here');/define('DB_PASSWORD', 'nasp18');/" /usr/share/nginx/html/wp-config.php

#Make a wordpress upload folder
mkdir -p /usr/share/nginx/html/wp-content/uploads

#Change the ownership of the nginx document root and its files
chown -R admin:nginx /usr/share/nginx/html/

#Get the first run mariadb config files
wget -P /usr/lib/systemd/system/ https://bitbucket.org/aboveone/cm_cloud_ops/raw/master/pxe_ks_setup/wp_mariadb_config.service
wget -P /root/ https://bitbucket.org/aboveone/cm_cloud_ops/raw/master/pxe_ks_setup/wp_mariadb_config.sh

#Grant executable permission on wp_mariadb_config.sh
chmod 755 /root/wp_mariadb_config.sh

#Enable the wp_mariadb_config service
ln -s /usr/lib/systemd/system/wp_mariadb_config.service /etc/systemd/system/multi-user.target.wants/wp_mariadb_config.service

%end
###### End Post-Installation Script ####################################################