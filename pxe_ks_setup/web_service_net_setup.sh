#!/bin/bash

# Make sure root user is not in use
if [[ "$EUID" -eq 0 ]]; then
  echo "Sorry, you cannot run this script as root."
  exit 1
fi

# The Network
network_name=nasp_cm_co
network_address=192.168.254.0
cidr_bits=24

# Port Forwarding
rule_name1=ssh
rule_name2=http
rule_name3=https
rule_name4=pxessh
protocol=tcp
global_ip=""
global_port1=50022
global_port2=80
global_port3=443
local_ip=192.168.254.10
local_port1=22
local_port2=80
local_port3=443
pxe_ip=192.168.254.5
pxe_port=50222

# VM Settings
vm_name=NASP_CM
vms_folder=~/cm_cloud_ops_vdisks
size_in_mb=10240 # Storage Disk Size
vm_group=nasp_cm_co
memory_mb=1024 # Memory Size

# VM Storage Controller
ctrlr_name1=IDE
ctrl_type1=ide
ctrlr_name2=SATA
ctrl_type2=sata
port_num0=0
device_num0=0
port_num1=1
device_num1=1

vboxmanage natnetwork add --netname $network_name --network "$network_address/$cidr_bits" --dhcp off

vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name1:$protocol:[$global_ip]:$global_port1:[$local_ip]:$local_port1"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name2:$protocol:[$global_ip]:$global_port2:[$local_ip]:$local_port2"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name3:$protocol:[$global_ip]:$global_port3:[$local_ip]:$local_port3"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name4:$protocol:[$global_ip]:$pxe_port:[$pxe_ip]:$local_port1"

sudo firewall-cmd --zone=$zone --add-port=$global_port1/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$global_port2/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$global_port3/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$pxe_port/tcp --permanent