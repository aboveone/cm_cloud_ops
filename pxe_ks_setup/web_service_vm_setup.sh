#!/bin/bash

# Make sure root user is not in use
if [[ "$EUID" -eq 0 ]]; then
  echo "Sorry, you cannot run this script as root."
  exit 1
fi

# The Network
network_name=nasp_cm_co
network_address=192.168.254.0
cidr_bits=24

# Port Forwarding
rule_name1=ssh
rule_name2=http
rule_name3=https
rule_name4=pxessh
protocol=tcp
global_ip=""
global_port1=50022
global_port2=80
global_port3=443
local_ip=192.168.254.10
local_port1=22
local_port2=80
local_port3=443
pxe_ip=192.168.254.5
pxe_port=50222

# VM Settings
vm_name=cm_co_wp_server
vms_folder=~/cm_cloud_ops_vdisks
size_in_mb=10240 # Storage Disk Size
vm_group=nasp_cm_co
memory_mb=1024 # Memory Size

# VM Storage Controller
ctrlr_name1=IDE
ctrl_type1=ide
ctrlr_name2=SATA
ctrl_type2=sata
port_num0=0
device_num0=0
port_num1=1
device_num1=1

# The ISO Locations
iso_file_path="/backups/ISOs/CentOS-7-x86_64-Minimal-1511.iso"
vbox_iso_file_path="/usr/share/virtualbox/VBoxGuestAdditions.iso"

# Create the VMs folder if not exist
if ! [ -d "$vms_folder" ]; then
    mkdir -p $vms_folder
fi

# Create the VM
vboxmanage createvm --name $vm_name --basefolder $vms_folder --register
vboxmanage createhd --filename ${vms_folder}/${vm_name}.vdi --size $size_in_mb --variant Standard

vboxmanage storagectl $vm_name --name $ctrlr_name1 --add $ctrl_type1 --bootable on
vboxmanage storagectl $vm_name --name $ctrlr_name2 --add $ctrl_type2 --bootable on

vboxmanage storageattach $vm_name --storagectl $ctrlr_name1 --port $port_num0 --device $device_num0 --type dvddrive --medium $iso_file_path
vboxmanage storageattach $vm_name --storagectl $ctrlr_name1 --port $port_num1 --device $device_num1 --type dvddrive --medium $vbox_iso_file_path
vboxmanage storageattach $vm_name --storagectl $ctrlr_name2 --port $port_num0 --device $device_num0 --type hdd --medium ${vms_folder}/${vm_name}.vdi --nonrotational on

vboxmanage modifyvm $vm_name\
    --groups "/${vm_group}"\
    --ostype "RedHat_64"\
    --cpus 1\
    --hwvirtex on\
    --nestedpaging on\
    --largepages on\
    --firmware BIOS\
    --nic1 natnetwork\
    --nictype1 "82543gc"\
    --nat-network1 "$network_name"\
    --cableconnected1 on\
    --audio none\
    --boot1 disk\
    --boot2 net\
    --boot3 dvd\
    --boot4 none\
    --memory "$memory_mb"

vboxmanage startvm $vm_name --type gui