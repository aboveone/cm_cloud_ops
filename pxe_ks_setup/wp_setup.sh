#!/bin/bash
if [[ "$EUID" -ne 0 ]]; then
    echo "Sorry, you need to run this as root."
    exit 1
fi

if ! [[ -e /etc/cm_update.lock ]] || ! [[ $(</etc/cm_update.lock) -eq 1 ]]; then
    yum -y install epel-release
    yum update -y

    useradd -m -G wheel,users admin
    echo "admin ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
    mkdir -p ~admin/.ssh/
    mkdir -p /root/.ssh/
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCu25cbwUSf2jqveyo/jOle1U2c4V0VXgKYMS9G/374TLxcslzPp2rvPSXYiQIibVSqBv/thvxs8iRm9uLNtd3dwD8Npb/RfXd/I0upoMdMSj/1cDQoY/Rype/JLlaBCdv9UIZeJaur/Ddr0ZdBS7ftMmrOow3A4Tv6cejC+D/wMHr2HVi7lb0zS8vewhhCQSjbx6t+2MxU8c7xfEy944abc6AIHIixJjVo0ETivC9+GPQopF7fWFfrUuErf+1CRerTX3MvsSWSVvdzfvjqnDkW7BAQUsYaWdh6ladXBkxua32UCiqXNwusmXzyeSCVNh8Zt+yi1yT3ZQvHZlW4YWehMQGKxCSLJkkPCcITCkX4l02cs2OMo6Fd5bwggdoXRv1BY9o2/3FXHdYry+oampOyORUYijo6hUs7BbcEUlKMp+LCdr+vOwAjlvKZ5NfgfOxUVAvwcO89fSteYSmd5i6+VNVjBpytXNshMLZA9XZN6fBuYYsL4rf6IWvbWbsrgRzcmas4lcR+UB4SkPTVPAqIiQ0sYENwT03g2wXDHjEdLEVjDnDi9ib8hnl/J1ZeAbVFjFKN8hvP6VCe1tBoWeHmxoDKRsF85dCYpVCaqTi0B4Mbs78Ew0w9bh7GYVSgRkJahXDu9qUOAyuuE0WQRgDvCtduIygFpHNdiX3FxrK7IQ== nasp18_admin" > ~admin/.ssh/authorized_keys
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCu25cbwUSf2jqveyo/jOle1U2c4V0VXgKYMS9G/374TLxcslzPp2rvPSXYiQIibVSqBv/thvxs8iRm9uLNtd3dwD8Npb/RfXd/I0upoMdMSj/1cDQoY/Rype/JLlaBCdv9UIZeJaur/Ddr0ZdBS7ftMmrOow3A4Tv6cejC+D/wMHr2HVi7lb0zS8vewhhCQSjbx6t+2MxU8c7xfEy944abc6AIHIixJjVo0ETivC9+GPQopF7fWFfrUuErf+1CRerTX3MvsSWSVvdzfvjqnDkW7BAQUsYaWdh6ladXBkxua32UCiqXNwusmXzyeSCVNh8Zt+yi1yT3ZQvHZlW4YWehMQGKxCSLJkkPCcITCkX4l02cs2OMo6Fd5bwggdoXRv1BY9o2/3FXHdYry+oampOyORUYijo6hUs7BbcEUlKMp+LCdr+vOwAjlvKZ5NfgfOxUVAvwcO89fSteYSmd5i6+VNVjBpytXNshMLZA9XZN6fBuYYsL4rf6IWvbWbsrgRzcmas4lcR+UB4SkPTVPAqIiQ0sYENwT03g2wXDHjEdLEVjDnDi9ib8hnl/J1ZeAbVFjFKN8hvP6VCe1tBoWeHmxoDKRsF85dCYpVCaqTi0B4Mbs78Ew0w9bh7GYVSgRkJahXDu9qUOAyuuE0WQRgDvCtduIygFpHNdiX3FxrK7IQ== nasp18_admin" > /root/.ssh/authorized_keys
    chown -R admin:admin ~admin/.ssh

    echo 1 > /etc/cm_update.lock
    echo "System rebooting, re-run this script after boot to continue."
    reboot
else
    echo "group_package_types=mandatory,default,optional" >> /etc/yum.conf
    yum -y group install base

    yum -y install curl vim nano wget expect bzip2 tmux nmap-ncat tcpdump nmap git iptables iptables-services iptables-utils nginx mariadb-server mariadb php php-mysql php-fpm kernel-devel kernel-headers dkms gcc gcc-c+
    timedatectl set-timezone America/Vancouver

    mkdir vbox_cd
    mount /dev/cdrom ./vbox_cd
    ./vbox_cd/VBoxLinuxAdditions.run
    umount ./vbox_cd
    rmdir ./vbox_cd

    systemctl enable nginx
    systemctl start nginx
    systemctl enable mariadb
    systemctl start mariadb
    systemctl enable php-fpm
    systemctl start php-fpm

SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root (enter for none):\"
send \"\r\"
expect \"Set root password?\"
send \"y\r\"
expect \"New password:\"
send \"nasp18\r\"
expect \"Re-enter new password:\"
send \"nasp18\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")

    echo "$SECURE_MYSQL"

    sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php.ini
    sed -i "s/listen = 127.0.0.1:9000/listen = \/var\/run\/php-fpm\/php-fpm.sock/" /etc/php-fpm.d/www.conf
    sed -i "s/;listen.owner = nobody/listen.owner = nobody/" /etc/php-fpm.d/www.conf
    sed -i "s/;listen.group = nobody/listen.group = nobody/" /etc/php-fpm.d/www.conf
    sed -i "s/user = apache/user = nginx/" /etc/php-fpm.d/www.conf
    sed -i "s/group = apache/group = nginx/" /etc/php-fpm.d/www.conf

    cat > /etc/nginx/nginx.conf <<"EOF"
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
        index index.php index.html index.htm;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
        location ~ \.php$ {
            try_files $uri =404;
            fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }
    }
}
EOF

    systemctl restart nginx
    systemctl restart php-fpm

    echo "<?php phpinfo(); ?>" > /usr/share/nginx/html/info.php

    mysql -u root -pnasp18 <<EOD
CREATE DATABASE wordpress;
CREATE USER wordpress_user@localhost IDENTIFIED BY 'nasp18';
GRANT ALL PRIVILEGES ON wordpress.* TO wordpress_user@localhost;
FLUSH PRIVILEGES;
EOD

    wget -O - http://wordpress.org/latest.tar.gz | tar zxf - --strip 1 -C /usr/share/nginx/html/
    cp /usr/share/nginx/html/wp-config-sample.php /usr/share/nginx/html/wp-config.php

    sed -i "s/define('DB_NAME', 'database_name_here');/define('DB_NAME', 'wordpress');/" /usr/share/nginx/html/wp-config.php
    sed -i "s/define('DB_USER', 'username_here');/define('DB_USER', 'wordpress_user');/" /usr/share/nginx/html/wp-config.php
    sed -i "s/define('DB_PASSWORD', 'password_here');/define('DB_PASSWORD', 'nasp18');/" /usr/share/nginx/html/wp-config.php

    mkdir -p /usr/share/nginx/html/wp-content/uploads
    chown -R admin:nginx /usr/share/nginx/html/

    rm -f /etc/cm_update.lock

    clear
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo " All Done! "
    echo " Wordpress is installed correctly. "
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
fi