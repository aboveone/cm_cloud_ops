#!/bin/bash - 
#===============================================================================
#
#          FILE: create_installer_iso.sh
# 
#         USAGE: ./create_installer_iso.sh 
# 
#   DESCRIPTION: Creates a CentOS installer ISO that incorporates:
#                * custom EFI/BOOT/grub.cfg that specifies a kickstart file
#                * kickstart file
#                * the creation and population of a setup directory on the iso
#                  this directory stores any files used in pre / post installation
#                  scripts within the kickstart installation
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Must be run as root since mounting to loop requires this.
#                Also must be run from the directory that holds the script
#        AUTHOR: Thomas Lane
#  ORGANIZATION: BCIT
#       CREATED: 25/07/16 12:52
#===============================================================================
#TODO update with root user check
set -o nounset                              # Treat unset variables as an error

#get the current directory of the script to setup relative paths
declare script_dir=$(dirname $0)

declare mirror_site="http://centos.mirror.iweb.ca/7/isos/x86_64/"
declare iso_name="CentOS-7-x86_64-Minimal-1511"
declare software_dir="${script_dir}/../software"
declare iso_mount="${script_dir}/iso_mount"
declare iso_content="${script_dir}/iso_content"
declare kickstart_file="${script_dir}/../pxe_server_config/kickstart/pxe_server.ks"
declare setup_content="${script_dir}/../pxe_server_config/setup"

#Verify script is being run as root
if [[ "$EUID" -ne 0 ]]; then
    echo "ERROR: Sorry, you need to run this as root"
    exit 1
fi

#Create a place to store ISO's if you don't have one
if [[ ! -d $software_dir ]] ; then
  mkdir $software_dir
fi

#Download Install ISO if you don't already have it
if [[ ! -f $software_dir/$iso_name.iso ]] ; then
  curl ${mirror_site}/${iso_name}.iso --output ${software_dir}/${iso_name}.iso
fi

#Create a place mount the installation ISO if you don't have one
if [[ -d $iso_mount ]] ; then
  rm -rf $iso_mount
fi

mkdir $iso_mount

if [[ -d $iso_content ]] ; then
  rm -rf $iso_content
fi


#Generate writable copy of ISO contents
mount -t iso9660 -o loop,ro $software_dir/$iso_name.iso $iso_mount
cp -pRf $iso_mount $iso_content

#Cleanup ISO: unmount and delete directory
umount $iso_mount
rm -rf $iso_mount

#Make modifications to ISO content
cp ${kickstart_file} $iso_content/kickstart.ks
cp ./iso_updates/grub.cfg $iso_content/EFI/BOOT/grub.cfg

#Copy files into Setup directory for use by kickstart file
mkdir $iso_content/setup
cp ../ssh_setup/authorized_keys $iso_content/setup/
cp ${setup_content}/* $iso_content/setup/

#Generate New ISO image 
genisoimage -U -r -v -T -J -joliet-long\
  -V "CentOS 7 x86_64"\
  -volset "CentOS 7 x86_64"\
  -A "CentOS 7 x86_64"\
  -b isolinux/isolinux.bin\
  -c isolinux/boot.cat\
  -no-emul-boot\
  -boot-load-size 4\
  -boot-info-table\
  -eltorito-alt-boot\
  -e images/efiboot.img\
  -no-emul-boot\
  -o ${software_dir}/${iso_name}_w_ks.iso $iso_content

#Notes:
# -U - allows the use of untranslated file names
# -r -sets the uid, and gid to zero for all files, zeros all write bits, 
#     sets read, and executable bits on all files
# -v - verbose exectution
# -T - generate a translation table  TRANS.TBL for use in certain systems
# -J - generate joliet directory records in addition to ISO file names - useful on Windows machines
# -jolient-long - allow file names up to 103 unicode chagracters.
# -V volume_id - specifies the volume id used by Solaris, MacOS, Windows 
# -volset volset_id - specifies the volume set it
# -b eltorito_boot_image - used to provide bootable components on cd image
# -c boot_catalog - needed for bootable cd
# -no-emul-boot - don't attempt to emulate a disk drive
# -boot-load-size 4 - specifieds the number of "virtual" sectors to load
# -boot-info-table specifies the creation of a 56-byte table of CD ROM layout will be patched in at offeset 8 of the boot file. 
# -eltorito-alt-boot - Start with a new set of El Torito boot parameters
# -e efi_boot_file - EFI boot file name
# -no-emul-boot - specifies that no translations will be performed on boot image
# -o output_file_name - path to output iso


#Remove all iso_content
rm -rf $iso_content
