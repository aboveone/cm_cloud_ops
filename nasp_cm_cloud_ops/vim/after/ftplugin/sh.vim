"Configuration for Bash Editing
setlocal number
setlocal autoindent
setlocal copyindent
setlocal tabstop=2
setlocal shiftwidth=2
setlocal expandtab
setlocal showmatch
setlocal title
setlocal smarttab
setlocal smartcase
setlocal nowrap

