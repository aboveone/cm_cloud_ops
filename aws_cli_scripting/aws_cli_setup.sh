#!/bin/bash -
if [[ -e ./state_file ]]; then
    rm -f ./state_file
fi
source ./aws_cli_vpc_setup.sh
source ./aws_cli_eip_setup.sh
source ./aws_cli_ec2_setup.sh