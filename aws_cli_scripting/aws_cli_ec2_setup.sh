#!/bin/bash -

# Get values from the output of VPC setup script and Elastic IP setup script
source ./state_file

centos_7_ami_id=ami-d2c924b2
instance_type=t2.micro
ssh_key_name=syu_ec2_key
instance_ip=172.16.2.101

# Get Instance ID
instance_id=$(aws ec2 run-instances \
    --image-id $centos_7_ami_id \
    --count 1 \
    --instance-type $instance_type \
    --block-device-mappings "DeviceName=/dev/sda1,Ebs={DeleteOnTermination=true}" \
    --key-name $ssh_key_name \
    --security-group-ids $security_group_id \
    --subnet-id $subnet_id \
    --private-ip-address $instance_ip \
    --user-data file://ec2_userdata.yml \
    --query 'Instances[*].InstanceId' \
    --output text)

# Get Instance State
while state=$(aws ec2 describe-instances \
    --instance-ids $instance_id \
    --query 'Reservations[*].Instances[*].State.Name' \
    --output text );\
    [[ $state = "pending" ]]; do
    echo -n '.' # Show we are working on something
    sleep 3s    # Wait three seconds before checking again
done
echo -e "\n$instance_id: $state"

# Associate Elastic IP with EC2 and get the Address Association ID
addr_association_id=$(aws ec2 associate-address \
    --instance-id $instance_id \
    --allocation-id $elastic_ip_allocation_id \
    --query AssociationId \
    --output text)