#!/bin/bash -
read -p "Would you like to clean up for re-run? [y/N] " clnup
case $clnup in
    y | yes)
    rm -f ./state_file
    echo "Clean up process completed!"
    exit 0
    ;;
    *)
    echo "Clean up process aborted!"
    exit 1
    ;;
esac