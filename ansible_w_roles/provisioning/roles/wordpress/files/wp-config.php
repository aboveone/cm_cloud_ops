<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress_user');

/** MySQL database password */
define('DB_PASSWORD', 'nasp18');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/Tm9zdUjb&|lK-SF^F]#?09HT&0daDdi/kj#bg3X>#y@SN?tBk}2+$xj3t5i@6Jj');
define('SECURE_AUTH_KEY',  'KLF;8Myu#`Pzjp&Rj|d82Vby1t+48clXfXW?e|Aw)X*UK7-G7l6$P)A0rQLom]!U');
define('LOGGED_IN_KEY',    'Suaf:#YnbpmF2u6@!QZDHN8%O&T.f?KncRKeO/q^v>is?K{Zy]cg3W4t-f!eo%g>');
define('NONCE_KEY',        '<|79=cV${*$%Jlb_K6*Ye/02H&yf[b0U@a>51hM+g@Nd3*?XL94Zt_|Zq`Ql(<z[');
define('AUTH_SALT',        'i%1UsEtc3jHk-f+PfYy.aBOMu$r=:cfPz*#nii|CU|d!v<VHQ/<IuJT>:bBxN?dP');
define('SECURE_AUTH_SALT', 'bqx%E]Ztn< |:,}8 ,AhG(no1LG6uJwnMfMoj_]Q1s.0|.} mb~5~=X]_a:4eaQN');
define('LOGGED_IN_SALT',   '4=evH;+&(bFD/y1ek+rKXv~QHNL,C6-p*|SF(eUYm:6#;Hjn@v_o0f2IX&+xXyIM');
define('NONCE_SALT',       'mf^j-i!E+4!j &T5-(iS>F^R>8qRy&SABDh0oj]&e7`6EzO@kZ)HV^zDuNLia)MF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
