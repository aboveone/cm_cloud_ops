#!/bin/bash -
#execute wp_mariadb_config.sql statements as the root mysql user, 
mysql -u root <<EOD
UPDATE mysql.user SET Password=PASSWORD('nasp18') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
CREATE DATABASE wordpress;
CREATE USER wordpress_user IDENTIFIED BY 'nasp18';
GRANT ALL PRIVILEGES ON wordpress.* TO wordpress_user;
FLUSH PRIVILEGES;
EOD