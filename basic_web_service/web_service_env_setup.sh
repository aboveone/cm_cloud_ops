#!/bin/bash
network_name=nasp_cm_co
network_address=192.168.254.0
cidr_bits=24
rule_name1=ssh_rule
rule_name2=http_rule
rule_name3=https_rule
rule_name4=pxe_ssh
protocol=tcp
global_ip=[]
zone=public
global_port1=50022
global_port2=50080
global_port3=50443
global_port4=50222
local_ip=192.168.254.10
pxe_ip=192.168.254.5
local_port1=22
local_port2=80
local_port3=443
vm_name=cm_co_wp_server
vms_folder="/home/syu/cm_cloud_ops_vdisks/"
size_in_mb=10240
ctrlr_name1=dvd
ctrl_type1=ide
ctrlr_name2=disk
ctrl_type2=sata
port_num0=0
port_num1=1
iso_file_path=/backups/ISOs/CentOS-7-x86_64-Minimal-1511.iso
memory_mb=1024
device_num0=0
device_num1=1
device_num2=0
group_name=/nasp_cm_co

vboxmanage natnetwork add --netname nasp_cm_co --network "192.168.254.0/24" --dhcp off


vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name1:$protocol:$global_ip:$global_port1:[$local_ip]:$local_port1"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name2:$protocol:$global_ip:$global_port2:[$local_ip]:$local_port2"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name3:$protocol:$global_ip:$global_port3:[$local_ip]:$local_port3"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name4:$protocol:$global_ip:$global_port4:[$pxe_ip]:$local_port1"


vboxmanage createvm --name $vm_name --basefolder $vms_folder --register

vboxmanage createhd --filename ${vms_folder}${vm_name}.vdi --size $size_in_mb -variant Standard

vboxmanage storagectl $vm_name --name $ctrlr_name1 --add $ctrl_type1 --bootable on
vboxmanage storagectl $vm_name --name $ctrlr_name2 --add $ctrl_type2 --bootable on

vboxmanage storageattach $vm_name --storagectl $ctrlr_name1 --port $port_num0 --device $device_num0 --type dvddrive --medium $iso_file_path

vboxmanage storageattach $vm_name --storagectl $ctrlr_name1 --port $port_num1 --device $device_num1 --type dvddrive --medium "/usr/share/virtualbox/VBoxGuestAdditions.iso"

vboxmanage storageattach $vm_name --storagectl $ctrlr_name2 --port $port_num0 --device $device_num2 --type hdd --medium ${vms_folder}${vm_name}.vdi --nonrotational on

vboxmanage modifyvm $vm_name\
    --ostype "RedHat_64"\
    --groups "$group_name"\
    --cpus 1\
    --hwvirtex on\
    --nestedpaging on\
    --largepages on\
    --firmware BIOS\
    --nic1 natnetwork\
    --macaddress1 020000000001\
    --nictype1 "82543GC"\
    --nat-network1 "$network_name"\
    --cableconnected1 on\
    --audio none\
    --boot1 disk\
    --boot2 net\
    --boot3 dvd\
    --boot4 none\
    --memory "$memory_mb"


vboxmanage startvm $vm_name --type gui

sudo firewall-cmd --zone=$zone --add-port=$global_port1/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$global_port2/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$global_port3/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$global_port4/tcp --permanent
sudo firewall-cmd --add-interface=em1 --permanent
sudo firewall-cmd --zone=public --permanent --add-service=http
sudo firewall-cmd --zone=public --permanent --add-service=https
sudo systemctl restart firewalld
